const WebSocket = require('ws');
const http = require('http').createServer();
const server = new WebSocket.Server({ server: http });
// const io = require('./server/node_modules/socket.io/dist')(http, {
//     cors: { origin: "*" }
// });



server.on('connection', socket => {
    socket.on('message', message => {
        socket.send(`Roger that! ${message}`);
    });
});

// io.on('connection', (socket) => {
//     console.log('a user connected');
//     socket.on('message', (message) => {
//         console.log(message);
//         io.emit('message', `${socket.id.substr(0, 2)} said: ${message}`);
//     });
// });

http.listen(8080, () => {
    console.log('HTTP server is running on port 8080');
});
